import { NextRequest } from 'next/server'
import { ZodError, z } from "zod";

const User = z.object({
  username: z.string().min(3).max(20),
  email: z.string().email(),
  password: z.string().min(6).max(100),
  token: z.string().min(6).max(100),
});

export async function POST(request: NextRequest) {
  try {
    const body = await request.json();
    User.parse(body);
    const { username, email, password, token } = body;
    console.log(username, email, password, token);
    
    const data = { message: 'Hello from Next.js Login!' };
    return new Response(JSON.stringify(data), {
      status: 200,
      headers: {
        'Content-Type': 'application/json',
      },
      statusText: 'OK',
    });
  } catch (errors) {
    if (errors instanceof ZodError) {
      return new Response(JSON.stringify({
        message: errors.issues,
      }), {
        status: 400,
        headers: {
          'Content-Type': 'application/json',
        },
        statusText: 'Bad Request',
      });
    }
    return new Response(JSON.stringify({
      message: 'Internal Server Error',
    }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
      statusText: 'Internal Server Error',
    });
  }
}