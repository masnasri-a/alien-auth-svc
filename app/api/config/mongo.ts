import * as mongoDB from "mongodb";
import * as dotenv from "dotenv";

export async function mongoConfig(coll: string) {
    dotenv.config();

    const client: mongoDB.MongoClient = new mongoDB.MongoClient(process.env.DB_CONN_STRING!);
    await client.connect();
    const db: mongoDB.Db = client.db(process.env.MONGODB_DATABASE);
    const collections: mongoDB.Collection = db.collection(coll);
    return [client, collections];
}