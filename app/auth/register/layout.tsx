import type { Metadata } from "next";
import { Poppins } from "next/font/google";
import "./styles.scss";

const poppins = Poppins({
    weight: ["400", "500", "600", "700"],
    subsets: ["latin"],
    preload: true,
});

export const metadata: Metadata = {
    title: "Alien Auth Login",
    description: "Alien Auth Login Page for Alien Auth App",
};

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
            <body className={poppins.className}>{children}</body>
        </html>
    );
}
