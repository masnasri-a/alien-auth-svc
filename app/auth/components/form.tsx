import React from 'react'
import { FcGoogle } from 'react-icons/fc'

const Form = (props: any) => {
    const token = props.token
    return (
        <div>
            <form className="w-full">
                <h1 className="text-[24px] font-medium mb-5">{props.type}</h1>
                <div className="menu">
                    <div className="input flex justify-center">
                        <div className="w-11/12">
                            <div className="w-full">
                                <label className="text-gray-600">Username</label>
                                <br />
                                <input
                                    type="text"
                                    placeholder='Enter your username'
                                    className="w-full p-2 border mb-[24px] mt-1 border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:placeholder-transparent"
                                />
                            </div>
                            {
                                props.type === "Register" ?
                                    <div className="w-full">
                                        <label className="text-gray-600">Email</label>
                                        <br />
                                        <input
                                            type="email"
                                            placeholder='Enter your email'
                                            className="w-full p-2 border mb-[24px] mt-1 border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:placeholder-transparent"
                                        />
                                    </div>
                                    :
                                    ""
                            }
                            <div className="w-full">
                                <label className="text-gray-600 mb-2">Password</label>
                                <br />
                                <input
                                    type="password"
                                    placeholder='Enter your password'
                                    className="w-full p-2 border mb-[6px] mt-1 border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:placeholder-transparent"
                                />
                            </div>
                            {
                                props.type === "Register" ?
                                    <div className="w-full mt-[18px] ">
                                        <label className="text-gray-600 mb-2 ">Confirm Password</label>
                                        <br />
                                        <input
                                            type="password"
                                            placeholder='Confirm your password'
                                            className="w-full p-2 border mb-[24px] mt-1 border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:placeholder-transparent"
                                        />
                                    </div>
                                    :
                                    <div className='mb-[24px]'>
                                        <input
                                            type="checkbox"
                                            className="mr-2"
                                        />
                                        <label className="text-gray-600">Remember me</label>
                                    </div>
                            }
                            <div className="w-full">
                                <button
                                    type="submit"
                                    className="w-full transition duration-300 delay-150 hover:delay-300 bg-[#2D81E0] hover:bg-[#5FC8F8] text-white p-2 rounded-md focus:outline-none"
                                >
                                    {props.type}
                                </button>
                            </div>
                            <div className="w-full xl:flex xl:justify-between text-center mt-5">
                                <span className="text-gray-600 mb-5 md:text-center">{
                                    props.type === "Login" ?
                                        "Don't have an account?"
                                        :
                                        "Already have an account?"
                                }
                                    {
                                        props.type === "Login" ?
                                            <a href={`/auth/register?token=${token}`} className="text-[#2D81E0] hover:underline"> Sign Up </a>
                                            :
                                            <a href={`/auth/login?token=${token}`} className="text-[#2D81E0] hover:underline"> Sign In </a>
                                    }
                                </span>
                                <p>
                                    {
                                        props.type === "Login" ?
                                            <a href="#" className="text-[#2D81E0] hover:underline">Forgot Password?</a>
                                            :
                                            ""
                                    }
                                </p>
                            </div>
                            <div className="w-full flex justify-center mt-5">
                                <button
                                    type="button"
                                    className="w-full flex items-center justify-center bg-white border border-gray-300 rounded-md shadow-sm px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                >
                                    <FcGoogle className="mr-2" />
                                    Sign in with Google
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default Form