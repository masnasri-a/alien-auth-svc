"use client"
import Image from "next/image";
import Logo from "@/public/logo.png";
import Bunny from "@/public/bunny.jpg";
import Form from "../components/form";
import { useRouter, useSearchParams } from "next/navigation";

export default function Home() {
    const token = useSearchParams().get('token')
    const router = useRouter()
    if (!token) {
        router.push('/not-found')
    }
    return (
        <main>
            <div className="row w-screen h-max-content xl:h-dvh xl:flex">
                <div className="left xl:w-1/2 md:w-screen">
                    <div className="bg-[#5FC8F8] xl:h-screen h-2/6">
                        <div className="logo">
                            <Image src={Logo} alt="Alien Logo" width={87} height={87} />
                        </div>
                        <div className="bunny fixed bottom-0 w-full flex justify-center bg-[#5FC8F8] xl:bg-transparent">
                            <Image src={Bunny} alt="Alien Bunny" className="h-[165px] w-[182px] sm:mr-[190px] z-" />
                        </div>
                    </div>
                </div>
                <div className="right xl:w-1/2 w-full mt-5 xl:mt-0 flex items-center justify-center  bg-white md:h-dvh">
                    <div className="flex flex-col justify-center h-max-content w-5/6 xl:w-4/6">
                    <Form type={"Login"} token={token}/>
                    </div>
                </div>
            </div>
        </main>
    );
}
